from django.shortcuts import render, get_object_or_404, redirect
from .models import Cliente
from .forms import ClienteForm

# Create your views here.

def list_clients(request):
    clientes = Cliente.objects.all()
    context = {
        'clientes' : clientes
    }
    return render(request, 'list_clients.html', context)

def view_client(request, id):
    view_client = get_object_or_404(Cliente, id = id)
    context = {
        'view_client' : view_client
    }
    return render(request, 'view_client.html', context)

def create_client(request):
    if request.method == 'POST':
        client_form = ClienteForm(request.POST)
        if client_form.is_valid():
            client_form.save()
            return redirect('aplication:inicio_cliente')
    else:
        client_form = ClienteForm()
    return render(request, 'create_client.html', {'client_form' : client_form})

def edit_client(request, id):
    edit_client = get_object_or_404(Cliente, id = id)
    if request.method == 'POST':
        client_form = ClienteForm(request.POST, instance = edit_client)
        if client_form.is_valid():
            client_form.save()
            return redirect('aplication:inicio_cliente')
    else:
        client_form = ClienteForm()
    return render(request, 'edit_client.html', {'client_form' : client_form})

def delete_client(request, id):
    delete_client = get_object_or_404(Cliente, id = id)
    if delete_client:
        delete_client.delete()
    return redirect('aplication:inicio_cliente')