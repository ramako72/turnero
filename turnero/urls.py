from django.urls import path
from .views import list_clients, view_client, create_client, edit_client, delete_client

app_name = 'aplication'

urlpatterns = [
    path('', list_clients, name='inicio_cliente'),
    path('detail_client/<int:id>/', view_client, name='ver_cliente'),
    path('create_client/', create_client, name='crear_cliente'),
    path('edit_client/<int:id>/', edit_client, name='editar_cliente'),
    path('delete_client/<int:id>/', delete_client, name='editar_cliente'),
]