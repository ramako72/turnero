from django.db import models

# Create your models here.
class Cliente(models.Model):
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    cedula = models.CharField(max_length=10, null=False, unique=True)
    PRIORIDADES = [
      ('a-alta', '3ra Edad, Embarazadas Discapacidad'),
      ('b-media', 'Clientes Corporativos'),
      ('c-baja', 'Clientes Personales'),
    ]
    cliente_prioridad = models.CharField(max_length=10, choices=PRIORIDADES, null=False)
    TIPO_CLIENTE=[
        ('P', 'Personal'),
        ('C', 'Corporativo'),
    ]
    TipoCliente = models.CharField(max_length=1, choices= TIPO_CLIENTE, null=False)

    def __str__(self) -> str:
        return self.cedula